package nl.han.aim.tourofheroes;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class HeroController {
    private HeroRepository heroRepository;

    public HeroController(HeroRepository heroRepository)
    {
        this.heroRepository = heroRepository;
    }

    @GetMapping("/api/heroes/{id}")
    public Hero getHero(@PathVariable Long id)
    {
        Optional<Hero> hero = heroRepository.findById(id);
        if (hero.isPresent()) return hero.get();
        else throw new HeroNotFoundException("Hero with id " + id + " cannot be found");
    }

    @GetMapping("/api/heroes")
    public List<Hero> searchHeroes(@RequestParam(required = false) String name)
    {
        if (name == null) return heroRepository.findAll();

        ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAny()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());

        return heroRepository.findAll(Example.of(Hero.from(name), customExampleMatcher));
    }

    @PostMapping("/api/heroes")
    public Hero addHero(@RequestBody Hero hero)
    {
        if (hero.getId() == null) hero.setId(heroRepository.findAll().size() + 1L);
        return heroRepository.save(hero);
    }

    @PutMapping("/api/heroes")
    public Hero updateHero(@RequestBody Hero hero) {
       return heroRepository.save(hero);
    }

    @DeleteMapping("/api/heroes/{id}")
    public void deleteHero(@PathVariable Long id){
        heroRepository.deleteById(id);
    }

}
