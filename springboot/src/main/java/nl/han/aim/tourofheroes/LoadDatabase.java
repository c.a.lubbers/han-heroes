package nl.han.aim.tourofheroes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(HeroRepository repository) {
        return args -> {
            repository.deleteAll();
            log.info("Preloading " + repository.save(new Hero(1L, "Rody")));
            log.info("Preloading " + repository.save(new Hero(2L, "Janneke")));
            log.info("Preloading " + repository.save(new Hero(3L, "Kai")));
            log.info("Preloading " + repository.save(new Hero(4L, "Teun")));
            log.info("Preloading " + repository.save(new Hero(5L, "Lucky")));
            log.info("Preloading " + repository.save(new Hero(6L, "Happy")));
        };
    }
}
