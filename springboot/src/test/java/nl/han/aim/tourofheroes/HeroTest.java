package nl.han.aim.tourofheroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    @Test
    public void fromMethodCreatesNewHeroWithoutId()
    {
        assertNull( Hero.from("A").getId());
    }

    @Test
    public void emptyConstructorLeavesBothFieldsNull()
    {
        Hero hero = new Hero();
        assertNull(hero.getId());
        assertNull(hero.getName());
    }

}
